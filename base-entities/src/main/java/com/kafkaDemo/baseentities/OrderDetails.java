package com.kafkaDemo.baseentities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetails {

	private String orderId;
	private String name;
	private int quantity;
	private double price;
	
}
