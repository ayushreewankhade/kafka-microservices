package com.kafkaDemo.baseentities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderEvent {   
//class used to tranfer the data between producer and consumer

	private String message;
	private String status;
	private OrderDetails order;
}
