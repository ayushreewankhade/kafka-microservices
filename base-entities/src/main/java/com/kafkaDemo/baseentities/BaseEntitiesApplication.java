package com.kafkaDemo.baseentities;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseEntitiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseEntitiesApplication.class, args);
	}

}
