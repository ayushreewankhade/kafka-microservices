package com.kafkaDemo.stockservice.kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.kafkaDemo.baseentities.OrderEvent;

@Service
public class OrderConsumer {

	 @KafkaListener(
	            topics = "${spring.kafka.topic.name}" //from .properties file
	            ,groupId = "${spring.kafka.consumer.group-id}"
	    )
	    public void consume(OrderEvent event){
		// save the order event into the database
		    
	    }
}
