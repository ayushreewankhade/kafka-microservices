package com.kafkaDemo.orderservice.controller;

import org.apache.kafka.common.Uuid;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kafkaDemo.baseentities.OrderDetails;
import com.kafkaDemo.baseentities.OrderEvent;
import com.kafkaDemo.orderservice.kafka.OrderProducer;

@RestController
public class OrderController {

	private OrderProducer producer;

	public OrderController(OrderProducer producer) {
		this.producer = producer;
	}
	
	@PostMapping("/orders")
	public String placeOrder(@RequestBody OrderDetails order) {
		//assign order id to order object
		order.setOrderId(Uuid.randomUuid().toString());
		
		OrderEvent event= new OrderEvent();
		event.setStatus("PENDING");
		event.setMessage("order status is pending");
		event.setOrder(order);
		
		producer.sendMessage(event);
		
		return "order placed";
	}
	
}
